//
//  PersonTableViewController.swift
//  TVC.CD.Swift
//
//  Created by Nayem BJIT on 3/10/17.
//  Copyright © 2017 Nayem BJIT. All rights reserved.
//

import UIKit
import CoreData

enum PLISTError: String, Error  {
    case NoData = "ERROR: No Data"
    case ConversionFailed = "ERROR: Conversion form plist failed"
}
enum CoreDataError: String, Error {
    case NoEntity = "ERROR: No Entity, Check the Entity Name"
}

enum JSONError: String, Error {
    case NoData = "ERROR: no data"
    case ConversionFailed = "ERROR: conversion from JSON failed"
}

typealias personJSONObjectType = [[String:Any]]

class PersonTableViewController: UITableViewController {
    var person: [Person] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Person List"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadPerson(from: Bundle.main)
//        self.loadPersonWithJSON(fromPath: "your json URL in String format")     // e.g. "https://myjsonapi.com"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.person.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let aPerson = self.person[indexPath.row]
        cell.textLabel?.text = aPerson.name
        cell.detailTextLabel?.text = aPerson.location
        return cell
    }
    
    // MARK:- Loading Data From Other Source
    
    func loadPerson(from bundle: Bundle) {
        
        guard let fileURL = bundle.url(forResource: "Persons", withExtension: "plist") else {
            print("No file named Mountains in \(bundle)")
            return
        }
        
        do {
            
            guard let data = try? Data(contentsOf: fileURL) else {
                throw PLISTError.NoData
            }
            
            guard let result = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [[String:Any]] else {
                throw PLISTError.ConversionFailed
            }
            
            /*** As you have Core Data Managed object model, you have to get your `Entity` ***/
            
            // First you need to have your shared App Delegate
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                print("No shared AppDelegate")
                return
            }
            
            // Use shared App Delegate to have the persistent containers view context as managed object context. This will be used to verify whether your Entity exists or not
            let managedObjectContext = appDelegate.persistentContainer.viewContext
            
            // Get the Entity in your core data model
            guard let entity = NSEntityDescription.entity(forEntityName: "Person", in: managedObjectContext) else {
                throw CoreDataError.NoEntity
            }
            
            let persons = result.map({ (personInfo) -> Person in
                
                let personName = personInfo["name"] as? String
                let personLocation = personInfo["location"] as? String
                
                // Get your object as Core data Managed object.
                let aPerson = NSManagedObject(entity: entity, insertInto: managedObjectContext) as! Person
                
                // Manipulate core data object with json data
                aPerson.name = personName
                aPerson.location = personLocation
                // Manipulation done
                
                return aPerson
            })
            
            person = persons
            
        } catch let error as PLISTError {
            print(error.rawValue)
        } catch let error as CoreDataError {
            print(error.rawValue)
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }
    
    func loadPersonWithJSON(fromPath jsonURLString:String) {
        guard let jsonURL = URL(string: jsonURLString) else {
            print("Error creating an URL from \(jsonURLString)")
            return
        }
        URLSession.shared.dataTask(with: jsonURL) { (data, response, error) in
            do {
                guard let data = data else {
                    throw JSONError.NoData
                }
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? personJSONObjectType else {
                    throw JSONError.ConversionFailed
                }
                
                // Here you have your json data. Now map this data to your model object.
                
                // First you need to have your shared App Delegate
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                    print("No shared AppDelegate")
                    return
                }
                
                // Use shared App Delegate to have the persistent containers view context as managed object context. This will be used to verify whether your Entity exists or not
                let managedObjectContext = appDelegate.persistentContainer.viewContext
                
                // Get the Entity in your core data model
                guard let entity = NSEntityDescription.entity(forEntityName: "Person", in: managedObjectContext) else {
                    throw CoreDataError.NoEntity
                }
                
                let persons = json.map({ (personInfo) -> Person in
                    
                    let personName = personInfo["name"] as? String              // use appropriate key for "name"
                    let personLocation = personInfo["location"] as? String      // use appropriate key for "location"
                    
                    // Get your object as Core data Managed object.
                    let aPerson = NSManagedObject(entity: entity, insertInto: managedObjectContext) as! Person
                    
                    // Manipulate core data object with json data
                    aPerson.name = personName
                    aPerson.location = personLocation
                    // Manipulation done
                    
                    return aPerson
                })
                
                self.person = persons
                self.tableView.reloadData()
                
            } catch let error as JSONError {
                print(error.rawValue)
            } catch let error as CoreDataError {
                print(error.rawValue)
            } catch let error as NSError {
                print(error.debugDescription)
            }
            }.resume()
    }
    
}

